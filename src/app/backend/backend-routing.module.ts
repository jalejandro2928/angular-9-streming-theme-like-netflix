import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'product',
        loadChildren: () =>
          import('./appBackoffice/admin-product/admin-product.module').then(m => m.AdminProductModule),
      },
      {
        path: 'users',
        loadChildren: () => import('./appBackoffice/admin-users/admin-users.module').then(m => m.AdminUsersModule),
      },
      {
        path: 'blog',
        loadChildren: () => import('./appBackoffice/blog-admin/blog-admin.module').then(m => m.BlogAdminModule),
      },
      {
        path: 'our-team',
        loadChildren: () =>
          import('./appBackoffice/admin-our-team/admin-our-team.module').then(m => m.AdminOurTeamModule),
      },
      {
        path: 'banners',
        loadChildren: () =>
          import('./appBackoffice/admin-banners/admin-banners.module').then(m => m.AdminBannersModule),
      },
      {
        path: 'big-banner',
        loadChildren: () =>
          import('./appBackoffice/admin-big-banner/admin-big-banner.module').then(m => m.AdminBigBannerModule),
      },
      {
        path: 'services',
        loadChildren: () =>
          import('./appBackoffice/admin-services/admin-services.module').then(m => m.AdminServicesModule),
      },
      {
        path: 'carrusel',
        loadChildren: () =>
          import('./appBackoffice/admin-carrusel/admin-carrusel.module').then(m => m.AdminCarruselModule),
      },
    ],
  },
  // {
  //   path: '**',
  //   redirectTo: 'product'
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BackendRoutingModule {}
