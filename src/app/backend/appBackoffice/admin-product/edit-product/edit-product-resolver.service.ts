import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ProductService } from 'src/app/backend/services/product/product.service';


@Injectable({
  providedIn: 'root'
})
export class EditProductResolverService {

  constructor(private productService: ProductService) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> {
    console.log('Entre Aqui')
    return this.productService.getProductAdminById(route.params['id']);
  }
}
