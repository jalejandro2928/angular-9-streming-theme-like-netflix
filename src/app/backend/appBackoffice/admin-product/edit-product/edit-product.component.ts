import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { BreadcrumbService } from '../../../common-layout-components/breadcrumd/service/breadcrumb.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { CategoriesService } from '../../../services/categories/catagories.service';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';

import { Router, ActivatedRoute } from '@angular/router';
import { IUser } from 'src/app/core/classes/user.class';
import { Subject } from 'rxjs';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { takeUntil, map } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ShowToastrService } from 'src/app/core/services/show-toastr/show-toastr.service';
import { ProductService } from 'src/app/backend/services/product/product.service';
import { ConfirmationDialogComponent } from 'src/app/backend/common-dialogs-module/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class EditProductComponent implements OnInit, OnDestroy {
  Brands: any[] = [];
  Categories: any[] = [];
  imageUrl = null;
  loggedInUser: IUser = null;
  language = null;
  _unsubscribeAll: Subject<any>;
  languages: any[] = [];
  languageForm: FormControl;
  BasicInfo: FormGroup;
  firstStateChanged = false;
  selectedProduct: any = null;
  recomendedProducts: any[] = [];
  tags: any[] = [];
  imagesProduct: any[] = [];

  shippings: any[] = [];
  taxes: any[] = [];

  ///////////////////////////////////////////////////////////////
  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private spinner: NgxSpinnerService,
    public utilsService: UtilsService,
    private activatedRoute: ActivatedRoute,
    private productService: ProductService,
    private categoriesService: CategoriesService,
    private loggedInUserService: LoggedInUserService,
    private fb: FormBuilder,
    private showToastr: ShowToastrService,
    public dialog: MatDialog,

    private showToast: ShowToastrService,
  ) {
    this._unsubscribeAll = new Subject<any>();
    this.loggedInUser = this.loggedInUserService.getLoggedInUser();
    this.languages = this.loggedInUserService.getlaguages();
    this.imageUrl = environment.imageUrl;
    // ------------------------------------------------
    this.language = this.loggedInUserService.getLanguage() ? this.loggedInUserService.getLanguage().lang : 'es';
    this.languageForm = new FormControl(
      this.loggedInUserService.getLanguage() ? this.loggedInUserService.getLanguage() : this.languages[0],
    );
    // -------------------------------------------------

    this.activatedRoute.data
      .pipe(
        takeUntil(this._unsubscribeAll),
        map((item) => item.product.data),
      )
      .subscribe((data) => {
        this.selectedProduct = data;
        this.imagesProduct = this.selectedProduct.Image;
        this.tags = this.selectedProduct.tags;
      });
  }

  ngOnInit() {
    this.breadcrumbService.clearBreadcrumd();
    this.breadcrumbService.setBreadcrumd('Produtos', false, '/backend/product/list');
    this.breadcrumbService.setBreadcrumd('Editar', false, null);
    this.breadcrumbService.setBreadcrumd(`${this.selectedProduct.id}`, true, null);

    this.buildForm();
    this.categoriesService.getAllBrands().subscribe((data) => {
      this.Brands = data.data;
    });
    this.categoriesService.getAllCategories().subscribe((data) => {
      this.Categories = data.data;
    });

    /////////Subscriptions to change data//////////

    this.languageForm.valueChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe((data) => {
      this.language = data.lang;
    });

    this.BasicInfo.controls['price'].valueChanges.subscribe((data) => {
      this.BasicInfo.controls['salePrice'].setValue(data);
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  buildForm(): void {
    this.BasicInfo = this.fb.group({
      name: [this.selectedProduct ? this.selectedProduct.name : null, Validators.required],
      description: [this.selectedProduct ? this.selectedProduct.description : null, Validators.required],
      BrandId: [this.selectedProduct ? this.selectedProduct.BrandId : null, Validators.required],
      CategoryIds: [
        this.selectedProduct ? this.selectedProduct.Categories.map((item) => item.id) : null,
        Validators.required,
      ],
      price: [this.selectedProduct ? this.selectedProduct.price : 0, [Validators.required]],
      rating: [this.selectedProduct ? this.selectedProduct.rating : null, []],
      salePrice: [this.selectedProduct ? this.selectedProduct.price : 0, [Validators.required]],
      stock: [
        this.selectedProduct ? this.selectedProduct.stock : 0,
        [Validators.required, Validators.min(0), Validators.max(10000)],
      ],
      height: [this.selectedProduct ? this.selectedProduct.height : 0, [Validators.required, Validators.min(0)]],
      width: [this.selectedProduct ? this.selectedProduct.width : 0, [Validators.required, Validators.min(0)]],
      length: [this.selectedProduct ? this.selectedProduct.length : 0, [Validators.required, Validators.min(0)]],
      weigth: [this.selectedProduct ? this.selectedProduct.weigth : 0, [Validators.required, Validators.min(0)]],
      stockThreshold: [
        this.selectedProduct ? this.selectedProduct.stockThreshold : 0,
        [Validators.required, Validators.min(0), Validators.max(50)],
      ],
      allowBackOrders: [this.selectedProduct ? this.selectedProduct.allowBackOrders : false, []],
      showStockQuantity: [this.selectedProduct ? this.selectedProduct.showStockQuantity : false, []],
    });
  }

  onSaveBasicProduct() {
    let data = this.BasicInfo.value;
    this.spinner.show();
    data.id = this.selectedProduct.id;
    this.productService.editProduct(data).subscribe(
      (data) => {
        this.showToastr.showSucces('Producto actualizado correctamente ', 8000);
        this.selectedProduct = data.data;
        this.spinner.hide();
        this.firstStateChanged = false;
      },
      (error) => {
        this.utilsService.errorHandle(error);
        this.spinner.hide();
      },
    );
  }

  // --------------------UTILS FOR LANGUAGE-----------------------
  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.lang === f2.lang;
  }

  parseLanguaje(data, lang) {
    data.name = { [lang]: data.name };
    data.description = { [lang]: data.description };
    return data;
  }

  parseLanguajeEdit(data, oldData, lang) {
    oldData.name[lang] = data.name;
    oldData.description[lang] = data.description;
    data.name = oldData.name;
    data.description = oldData.description;
    return data;
  }
  // -------------------------------------------------------------
}
