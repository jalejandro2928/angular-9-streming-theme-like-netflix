import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminListProductComponent } from './admin-list-product/admin-list-product.component';
import { AdminListCategoriesComponent } from './admin-list-categories/admin-list-categories.component';
import { AdminListBrandsComponent } from './admin-list-brands/admin-list-brands.component';
import { CreateProductComponent } from './create-product/create-product.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import { EditProductResolverService } from './edit-product/edit-product-resolver.service';



const routes: Routes = [
  {
    path: '',
    redirectTo: 'list'
  },
  {
    path: 'list',
    component: AdminListProductComponent,
  },
  {
    path: 'categories',
    component: AdminListCategoriesComponent,
  },
  {
    path: 'brands',
    component: AdminListBrandsComponent,
  },
  {
    path: 'create-product',
    component: CreateProductComponent,
  },
  {
    path: 'edit-product/:id',
    component: EditProductComponent,
    resolve: {
      product: EditProductResolverService
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminProductRoutingModule { }
