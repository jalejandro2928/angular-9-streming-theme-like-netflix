import { IPagination } from 'src/app/core/classes/pagination.class';
import { IUser } from 'src/app/core/classes/user.class';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Component, HostListener, OnInit, ViewChild, ViewEncapsulation, Input, OnDestroy } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { ShowToastrService } from 'src/app/core/services/show-toastr/show-toastr.service';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';

import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { BreadcrumbService } from '../../../common-layout-components/breadcrumd/service/breadcrumb.service';
import { Router } from '@angular/router';
import { StateCreatingProductService } from '../../../services/state-creating-product/state-creating-product.service';
import { ConfirmationDialogComponent } from 'src/app/backend/common-dialogs-module/confirmation-dialog/confirmation-dialog.component';
import { UserService } from 'src/app/backend/services/user/user.service';
import { ProductService } from 'src/app/backend/services/product/product.service';

@Component({
  selector: 'app-admin-list-product',
  templateUrl: './admin-list-product.component.html',
  styleUrls: ['./admin-list-product.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AdminListProductComponent implements OnInit, OnDestroy {
  @Input() role;
  urlImage: string;
  innerWidth: any;
  applyStyle = false;
  allProducts: any[] = [];
  searchForm: FormGroup;
  dataSource: MatTableDataSource<any>;
  showFilterProduct: boolean;
  loggedInUser: IUser;
  loading = false;
  _unsubscribeAll: Subject<any>;
  selection: SelectionModel<any>;
  imageUrl: any;
  showActionsBtn = false;
  displayedColumns: string[] = ['select', 'image', 'name', 'category', 'brand', 'price', 'actions'];
  pageSizeOptions: number[] = [10, 25, 100, 1000];
  searchElementCount = 0;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  language: 'es';

  isLoading = false;

  constructor(
    private fb: FormBuilder,
    private loggedInUserService: LoggedInUserService,
    private productService: ProductService,
    private breadcrumbService: BreadcrumbService,
    private showToastr: ShowToastrService,
    public dialog: MatDialog,
    public utilsService: UtilsService,
    private router: Router,
    private stateCreatingProduct: StateCreatingProductService,
  ) {
    this.dataSource = new MatTableDataSource([]);
    this.selection = new SelectionModel<any>(true, []);
    this.loggedInUser = this.loggedInUserService.getLoggedInUser();
    this.imageUrl = environment.imageUrl;

    this._unsubscribeAll = new Subject<any>();
    // ------------------------------------------------
    this.language = this.loggedInUserService.getLanguage() ? this.loggedInUserService.getLanguage().lang : 'es';
    // -------------------------------------------------
  }

  ngOnInit() {
    this.breadcrumbService.clearBreadcrumd();
    this.breadcrumbService.setBreadcrumd('Nuestros productos', true);

    this.refreshData();
    this.createSearchForm();

    this.searchForm.valueChanges.subscribe((val) => {
      const data = this.filterProducts(val.textCtrl);
      this.dataSource = new MatTableDataSource<any>(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    ///////////////////////////////////////////

    this.loggedInUserService.$languageChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: any) => {
      this.language = data.lang;
    });

    ///////////////////////////////////////////
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  refreshData(): void {
    this.isLoading = true;
    this.productService.getAllAdminProducts({ limit: 0, offset: 0 }, { role: this.role }).subscribe(
      (data) => {
        this.initTable(data.data);
        this.selection.clear();
        this.isLoading = false;
      },
      (error) => {
        this.utilsService.errorHandle(error);
        this.selection.clear();
        this.isLoading = false;
      },
    );
  }

  initTable(data) {
    this.allProducts = data;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createSearchForm() {
    this.searchForm = this.fb.group({
      textCtrl: ['', [Validators.required]],
    });
  }

  showSearchForm() {
    this.showFilterProduct = true;
  }

  hideSearchForm() {
    this.showFilterProduct = false;
    this.searchForm.controls['textCtrl'].setValue('');
  }

  filterProducts(searchValue: string) {
    const temp = this.allProducts.filter((product) => this._filterProduct(product, searchValue));
    return temp;
  }

  private _filterProduct(product, searchValue) {
    const nameParams = product.name[this.language] ? product.name[this.language] : product.name['es'];
    const brandParams = product.Brand.name[this.language]
      ? product.Brand.name[this.language]
      : product.Brand.name['es'];
    const priceParams = product.price + '';
    return (
      nameParams.toLowerCase().indexOf(searchValue.toLowerCase()) >= 0 ||
      brandParams.toLowerCase().indexOf(searchValue.toLowerCase()) >= 0 ||
      priceParams.toLowerCase().indexOf(searchValue.toLowerCase()) >= 0
    );
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth > 600) {
      this.applyStyle = false;
    } else {
      this.applyStyle = true;
    }
  }

  onCreateProduct(): void {
    this.router.navigate(['backend/product/create-product']);
  }

  onEditProduct(product): void {
    this.router.navigate(['backend/product/edit-product', product.id]);
  }

  onFinishCreatingProduct(element) {
    this.stateCreatingProduct.setProducCreated(element);
    this.router.navigate(['backend/product/create-product']);
  }

  async onRemoveProduct(products) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '450px',
      data: {
        title: 'Confirmación',
        question: '¿Está seguro que desea eliminar el producto?',
      },
    });

    dialogRef.afterClosed().subscribe(async (result) => {
      try {
        if (result) {
          await Promise.all(products.map((item) => this.productService.removeProduct(item)));
          this.showToastr.showSucces('Products successfully removed', 5000);
          this.refreshData();
        }
      } catch (error) {
        this.utilsService.errorHandle(error);
        this.refreshData();
      }
    });
  }
  public onChangeSorting(val) {
    let value = val.split('-');
    let orden = 1;
    let field = null;
    if (value.length > 1) {
      orden = 1;
      field = value[1];
    } else {
      orden = -1;
      field = value[0];
    }
    this.allProducts = this.allProducts.sort(function (a, b) {
      if (a[field] < b[field]) {
        return orden;
      }
      if (a[field] > b[field]) {
        return -1 * orden;
      } else {
        return 0;
      }
    });
    console.log('AdminListProductComponent -> onChangeSorting -> this.allProducts', this.allProducts);
  }
}
