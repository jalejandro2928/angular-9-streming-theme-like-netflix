import { ShowToastrService } from 'src/app/core/services/show-toastr/show-toastr.service';
import { Component, OnInit, ViewEncapsulation, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { BreadcrumbService } from '../../../common-layout-components/breadcrumd/service/breadcrumb.service';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';

import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CategoriesService } from '../../../services/categories/catagories.service';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';
import { IUser } from 'src/app/core/classes/user.class';
import { environment } from 'src/environments/environment';

import { StateCreatingProductService } from '../../../services/state-creating-product/state-creating-product.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/backend/services/product/product.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CreateProductComponent implements OnInit, OnDestroy {
  Brands: any[] = [];
  Categories: any[] = [];
  stepIndex = 0;
  BasicInfo: FormGroup;
  ProductData: FormGroup;
  imagesDataProduct: any[] = [];
  typesProducts = ['Simple', 'Grouped', 'Virtual', 'Downloadable'];
  typesTaxes = ['Taxable', 'Shiping Only', 'None'];
  typesClasses = [];
  typesShipping = [];
  stepPass = 0;
  imageUrl = null;
  loggedInUser: IUser = null;
  productCreated: any = null;
  language = null;
  _unsubscribeAll: Subject<any>;
  languages: any[] = [];
  languageForm: FormControl;
  firstStateChanged = false;
  recomendedProducts: any[] = [];
  recomendedProductsOutput: any[] = [];
  tags: any[] = [];
  @ViewChild('stepper', { static: false }) stepper: any;

  ///////////////////////////////////////////////////////////////
  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private spinner: NgxSpinnerService,
    public utilsService: UtilsService,
    private productService: ProductService,
    private categoriesService: CategoriesService,
    private loggedInUserService: LoggedInUserService,
    private showToastr: ShowToastrService,
    private stateProduct: StateCreatingProductService,
    private router: Router,
    private fb: FormBuilder,
  ) {
    this._unsubscribeAll = new Subject<any>();
    this.loggedInUser = this.loggedInUserService.getLoggedInUser();
    this.languages = this.loggedInUserService.getlaguages();
    this.imageUrl = environment.imageUrl;
    this.productCreated = this.stateProduct.getProductCreated();

    const tempLang = this.languages.find((item) => item.lang === 'es');
    this.languageForm = new FormControl(tempLang, []);
    this.language = 'es';
  }

  ngOnInit() {
    this.breadcrumbService.clearBreadcrumd();
    this.breadcrumbService.setBreadcrumd('Products', false, '/backend/product/list');
    this.breadcrumbService.setBreadcrumd('Crear producto', true);

    this.buildForm();
    this.categoriesService.getAllBrands().subscribe((data) => {
      this.Brands = data.data;
    });
    this.categoriesService.getAllCategories().subscribe((data) => {
      this.Categories = data.data;
    });

    /////////Subscriptions to change data//////////

    this.BasicInfo.controls['price'].valueChanges.subscribe((data) => {
      this.BasicInfo.controls['salePrice'].setValue(data);
    });

    this.languageForm.valueChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe((data) => {
      this.language = data.lang;
    });

    this.BasicInfo.valueChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe(() => {
      this.firstStateChanged = true;
    });

    /////////////////////////////////////////////
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  buildForm(): void {
    let categories = null;
    if (this.productCreated && this.productCreated.Category) {
      categories = this.productCreated.Category.map((item) => item.id);
    }
    if (this.productCreated && this.productCreated.Categories) {
      categories = this.productCreated.Categories.map((item) => item.id);
    }

    this.BasicInfo = this.fb.group({
      name: [this.productCreated ? this.productCreated.name : null, Validators.required],
      description: [this.productCreated ? this.productCreated.description : null, Validators.required],
      BrandId: [this.productCreated ? this.productCreated.BrandId : null, Validators.required],
      CategoryIds: [categories, Validators.required],
      rating: [this.productCreated ? this.productCreated.rating : null, []],
      price: [this.productCreated ? this.productCreated.price : 0, [Validators.required]],
      salePrice: [this.productCreated ? this.productCreated.price : 0, [Validators.required]],
      stock: [
        this.productCreated ? this.productCreated.stock : 0,
        [Validators.required, Validators.min(0), Validators.max(10000)],
      ],
      height: [this.productCreated ? this.productCreated.height : 0, [Validators.required, Validators.min(0)]],
      width: [this.productCreated ? this.productCreated.width : 0, [Validators.required, Validators.min(0)]],
      length: [this.productCreated ? this.productCreated.length : 0, [Validators.required, Validators.min(0)]],
      weigth: [this.productCreated ? this.productCreated.weigth : 0, [Validators.required, Validators.min(0)]],
      stockThreshold: [
        this.productCreated ? this.productCreated.stockThreshold : 0,
        [Validators.required, Validators.min(0), Validators.max(50)],
      ],
      allowBackOrders: [this.productCreated ? this.productCreated.allowBackOrders : false, []],
      showStockQuantity: [this.productCreated ? this.productCreated.showStockQuantity : false, []],
    });
    this.ProductData = this.fb.group({
      taxType: ['None', [Validators.required]],
      taxClass: [null, []],
    });

    // if (this.productCreated) {
    //   this.productService
    //     .getRecomendedProduct(this.productCreated.id)
    //     .subscribe((data: any) => {
    //       this.recomendedProducts = data.data;
    //     });
    //   this.tags = this.productCreated.tags;
    // }
  }

  onArrayImagesChanged(event) {
    this.imagesDataProduct = event;
  }

  onClearStorage(): void {
    localStorage.removeItem('productCreated');
    this.productCreated = null;
    this.showToastr.showSucces('The state storage of the Product has been deleted', 6000);
    this.BasicInfo.reset();
    this.ProductData.reset();
  }

  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.lang === f2.lang;
  }

  onSaveBasicProduct(): void {
    if (this.firstStateChanged) {
      let data = this.BasicInfo.value;
      this.spinner.show();
      if (this.productCreated) {
        data.id = this.productCreated.id;
        this.productService.editProduct(data).subscribe(
          (data) => {
            this.showToastr.showSucces('Product has been edited successfully', 6000);
            this.productCreated = data.data;
            this.stepIndex += 1;
            this.stateProduct.setProducCreated(data.data);
            this.spinner.hide();
            this.firstStateChanged = false;
          },
          (error) => {
            this.utilsService.errorHandle(error);
            this.spinner.hide();
          },
        );
      } else {
        this.productService.createProduct(data).subscribe(
          (data) => {
            this.showToastr.showSucces('Product has been successfully created', 6000);
            this.productCreated = data.data;
            this.stepIndex += 1;
            this.stateProduct.setProducCreated(data.data);
            this.firstStateChanged = false;
            this.spinner.hide();
          },
          (error) => {
            this.utilsService.errorHandle(error);
            this.spinner.hide();
          },
        );
      }
    } else {
      this.stepIndex += 1;
    }
  }

  onSlectionChange(event): void {
    this.stepIndex = event.selectedIndex;
  }

  parseLanguaje(data, lang) {
    data.name = { [lang]: data.name };
    data.description = { [lang]: data.description };
    return data;
  }

  parseLanguajeEdit(data, olData, lang) {
    olData.name[lang] = data.name;
    olData.description[lang] = data.description;
    data.name = olData.name;
    data.description = olData.description;
    return data;
  }

  //////////////////FUNCTIONS RECOMENDED PRODUCTS///////////////////

  onRecomendedProductChange(event) {
    this.recomendedProductsOutput = event;
  }

  onSetRecomended(): void {
    const data = this.recomendedProductsOutput.map((item) => item.id);
    this.productService.createRecomendedProduct(this.productCreated.id, { ids: data }).subscribe(
      (newRecomended) => {
        this.showToastr.showSucces('The recomended products has been changed successfully');
      },
      (error) => {
        this.utilsService.errorHandle(error);
      },
    );
  }

  //////////////////////////////////////////////////////////////////////////

  ///////////////////FUNCTION TO TAGS//////////////////////

  onChangeTags(event) {
    if (this.productCreated) {
      this.productCreated.tags = event;
      this.stateProduct.setProducCreated(this.productCreated);
    }
  }

  ///////////////////////////////////////////////////////

  onSaveProduct(): void {
    this.productService.editProduct({ status: 'published', id: this.productCreated.id }).subscribe(
      () => {
        this.showToastr.showSucces('You have published your product successfully');
        this.onClearStorage();
        this.stepper.reset();
        this.router.navigate(['/backend/product/list']);
      },
      (error) => {
        this.utilsService.errorHandle(error);
      },
    );
  }

  getProgress() {
    let progress = 0;
    if (this.BasicInfo.valid) {
      progress += 25;
    }
    if (this.BasicInfo.valid && this.imagesDataProduct.length) {
      progress += 25;
    }
    if (this.BasicInfo.valid && this.imagesDataProduct.length && this.recomendedProducts) {
      progress += 25;
    }
    if (this.stepIndex === 3) {
      progress += 25;
    }
    return progress;
  }

  //////////////////////////////////////////////////////////////
}
