import { Component, Inject, HostListener, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators, FormGroupName } from '@angular/forms';
import { ShowToastrService } from 'src/app/core/services/show-toastr/show-toastr.service';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';
import { environment } from 'src/environments/environment';

import { NgxSpinnerService } from 'ngx-spinner';
import { UtilsService } from 'src/app/core/services/utils/utils.service';

import { CategoriesService } from '../../../../services/categories/catagories.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-dialog-add-edit-categories',
  templateUrl: './dialog-add-edit-categories.component.html',
  styleUrls: ['./dialog-add-edit-categories.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DialogAddEditCategoriesComponent implements OnInit, OnDestroy {
  isSaving = false;
  isEditing = false;
  loggedInUser: any;
  selectedCategory: any;
  innerWidth: any;
  applyStyle = false;
  form: FormGroup;
  Categories: any[] = [];

  languages: any[] = [];
  imageUrl: any;
  languageForm: FormControl;
  language: any;
  _unsubscribeAll: Subject<any>;
  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogAddEditCategoriesComponent>,
    private loggedInUserService: LoggedInUserService,
    private fb: FormBuilder,
    public spinner: NgxSpinnerService,
    public utilsService: UtilsService,
    private showToastr: ShowToastrService,
    private categoriesService: CategoriesService,
  ) {
    this.dialogRef.disableClose = true;
    this.loggedInUser = this.loggedInUserService.getLoggedInUser();
    this._unsubscribeAll = new Subject<any>();

    this.isEditing = data.isEditing;
    this.selectedCategory = data.selectedCategory;

    // ------------------LANGUAGE INITIALIZATION----------------
    this.languages = this.loggedInUserService.getlaguages();
    this.language = this.loggedInUserService.getLanguage() ? this.loggedInUserService.getLanguage().lang : 'es';
    this.languageForm = new FormControl(
      this.loggedInUserService.getLanguage() ? this.loggedInUserService.getLanguage() : this.languages[0],
    );
    // ---------
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth > 600) {
      this.applyStyle = false;
    } else {
      this.applyStyle = true;
    }
  }

  ngOnInit(): void {
    this.createForm();
    this.categoriesService.getAllCategories({ limit: 0, offset: 0 }).subscribe(
      (data) => {
        this.Categories = data.data;
      },
      (error) => {
        this.utilsService.errorHandle(error);
      },
    );
  }

  createForm(): void {
    if (this.isEditing) {
      this.form = this.fb.group({
        name: [
          this.selectedCategory && this.selectedCategory.name ? this.selectedCategory.name : null,
          [Validators.required],
        ],
        description: [
          this.selectedCategory && this.selectedCategory.description ? this.selectedCategory.description : [],
        ],
        ParentCategoryId: [
          this.selectedCategory && this.selectedCategory.ParentCategoryId
            ? this.selectedCategory.ParentCategoryId
            : null,
        ],
      });
    } else {
      this.form = this.fb.group({
        name: [null, [Validators.required]],
        description: [null, []],
        ParentCategoryId: [null, []],
      });
    }

    //////////////////EVENT ASSOCIATED WITH CHANGE LANGUAGE////////////////////////////
    this.languageForm.valueChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe((data) => {
      this.language = data.lang;
    });
    //////////////////////////////////////////////
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  //////////////////////////////////////////

  onSave(): void {
    this.spinner.show();
    let data = this.form.value;
    if (!data.ParentCategoryId) {
      delete data.ParentCategoryId;
    }

    if (!this.isEditing) {
      this.categoriesService.createCategory(data).subscribe(
        () => {
          this.showToastr.showSucces('Category successfully created');
          this.spinner.hide();
          this.dialogRef.close(true);
        },
        (error) => {
          console.log(error);
          this.utilsService.errorHandle(error, 'Category', 'Creating');
          this.spinner.hide();
        },
      );
    } else {
      data.id = this.selectedCategory.id;
      console.log(data);
      this.categoriesService.editCategory(data).subscribe(
        (newProfile) => {
          this.showToastr.showSucces('Category updated successfully');
          this.spinner.hide();
          this.dialogRef.close(true);
        },
        (error) => {
          console.log(error);
          this.utilsService.errorHandle(error, 'Category', 'Editing');
          this.spinner.hide();
        },
      );
    }
  }

  ////////////////////////////UTILS FOR LANGUAGE HANDLE///////////////////////////////////////
  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.lang === f2.lang;
  }

  parseLanguaje(data, lang) {
    data.name = { [lang]: data.name };
    data.description = { [lang]: data.description };
    return data;
  }

  parseLanguajeEdit(data, olData, lang) {
    olData.name[lang] = data.name;
    olData.description[lang] = data.description;
    data.name = olData.name;
    data.description = olData.description;
    return data;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
}
