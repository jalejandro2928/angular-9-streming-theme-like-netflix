import { IPagination } from 'src/app/core/classes/pagination.class';
import { IUser } from 'src/app/core/classes/user.class';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { Component, HostListener, OnInit, ViewChild, ViewEncapsulation, Input, OnDestroy } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { ShowToastrService } from 'src/app/core/services/show-toastr/show-toastr.service';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { environment } from 'src/environments/environment';

import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { BreadcrumbService } from '../../../common-layout-components/breadcrumd/service/breadcrumb.service';
import { DialogAddEditCategoriesComponent } from './dialog-add-edit-categories/dialog-add-edit-categories.component';
import { CategoriesService } from '../../../services/categories/catagories.service';
import { ConfirmationDialogComponent } from 'src/app/backend/common-dialogs-module/confirmation-dialog/confirmation-dialog.component';
import { ProductService } from 'src/app/backend/services/product/product.service';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-admin-list-categories',
  templateUrl: './admin-list-categories.component.html',
  styleUrls: ['./admin-list-categories.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AdminListCategoriesComponent implements OnInit, OnDestroy {
  @Input() role;
  urlImage: string;
  innerWidth: any;
  applyStyle = false;
  allCategories: any[] = [];
  searchForm: FormGroup;
  dataSource: MatTableDataSource<any>;
  showFilterCategory: boolean;
  loggedInUser: IUser;
  loading = false;
  _unsubscribeAll: Subject<any>;
  selection: SelectionModel<any>;
  imageUrl: any;
  showActionsBtn = false;
  displayedColumns: string[] = ['select', 'name', 'description', 'actions'];
  pageSizeOptions: number[] = [10, 25, 100, 1000];
  searchElementCount = 0;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  language: 'es';

  constructor(
    private fb: FormBuilder,
    private loggedInUserService: LoggedInUserService,
    private productService: ProductService,

    private breadcrumbService: BreadcrumbService,

    public dialog: MatDialog,
    private categoriesService: CategoriesService,
    public utilsService: UtilsService,
    private showToastr: ShowToastrService,
  ) {
    this.dataSource = new MatTableDataSource([]);
    this.selection = new SelectionModel<any>(true, []);
    this.loggedInUser = this.loggedInUserService.getLoggedInUser();
    this.imageUrl = environment.imageUrl;
    this._unsubscribeAll = new Subject<any>();

    // ------------------------------------------------
    this.language = this.loggedInUserService.getLanguage() ? this.loggedInUserService.getLanguage().lang : 'es';
    // -------------------------------------------------
  }

  ngOnInit() {
    this.breadcrumbService.clearBreadcrumd();
    this.breadcrumbService.setBreadcrumd('Nuestras categorías', true);

    this.refreshData();
    this.createSearchForm();

    this.searchForm.valueChanges.subscribe((val) => {
      const data = this.filterCategories(val.textCtrl);
      this.dataSource = new MatTableDataSource<any>(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    ///////////////////////////////////////////

    this.loggedInUserService.$languageChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: any) => {
      this.language = data.lang;
    });

    ///////////////////////////////////////////
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  refreshData(): void {
    this.selection.clear();
    this.categoriesService.getAllCategories({ limit: 0, offset: 0 }).subscribe(
      (data) => {
        this.initTable(data.data);
        this.selection.clear();
      },
      (error) => {
        this.utilsService.errorHandle(error);
        this.selection.clear();
      },
    );
  }

  initTable(data) {
    this.allCategories = data;
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  createSearchForm() {
    this.searchForm = this.fb.group({
      textCtrl: ['', [Validators.required]],
    });
  }

  showSearchForm() {
    this.showFilterCategory = true;
  }

  hideSearchForm() {
    this.showFilterCategory = false;
    this.searchForm.controls['textCtrl'].setValue('');
  }

  filterCategories(searchValue: string) {
    const temp = this.allCategories.filter((category) => this._filterCategories(category, searchValue));
    return temp;
  }

  private _filterCategories(category, searchValue) {
    const nameParams = category.name[this.language] ? category.name[this.language] : category.name['es'];
    return nameParams.toLowerCase().indexOf(searchValue.toLowerCase()) >= 0;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth > 600) {
      this.applyStyle = false;
    } else {
      this.applyStyle = true;
    }
  }

  ///////Select logic/////////////////

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach((row) => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  //////////////////////////////

  onCreateCategory(): void {
    let dialogRef: MatDialogRef<DialogAddEditCategoriesComponent, any>;
    dialogRef = this.dialog.open(DialogAddEditCategoriesComponent, {
      panelClass: 'app-dialog-add-edit-categories',
      maxWidth: '100vw',
      maxHeight: '100vh',
      data: {
        isEditing: false,
        selectedCategory: null,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.refreshData();
    });
  }

  onEditCategory(category): void {
    this.categoriesService.getCategory(category.id).subscribe((data) => {
      let dialogRef: MatDialogRef<DialogAddEditCategoriesComponent, any>;
      dialogRef = this.dialog.open(DialogAddEditCategoriesComponent, {
        panelClass: 'app-dialog-add-edit-categories',
        maxWidth: '100vw',
        maxHeight: '100vh',
        data: {
          isEditing: true,
          selectedCategory: data.data,
        },
      });

      dialogRef.afterClosed().subscribe((result) => {
        this.refreshData();
      });
    });
  }

  async onRemoveCategory(categories) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '450px',
      data: {
        title: 'Confirmación',
        question: '¿Está seguro que desea eliminar la categoria?',
      },
    });

    dialogRef.afterClosed().subscribe(async (result) => {
      try {
        if (result) {
          const data = await Promise.all(categories.map((item) => this.categoriesService.removeCategory(item)));
          this.showToastr.showSucces('Users successfully removed', 'Succes', 7500);
          this.refreshData();
        }
      } catch (error) {
        this.utilsService.errorHandle(error);
        this.refreshData();
      }
    });
  }
}
