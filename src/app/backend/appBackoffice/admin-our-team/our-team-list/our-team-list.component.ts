import { IPagination } from 'src/app/core/classes/pagination.class';
import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { BreadcrumbService } from '../../../common-layout-components/breadcrumd/service/breadcrumb.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { Subject } from 'rxjs';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';
import { takeUntil } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ConfirmationDialogComponent } from 'src/app/backend/common-dialogs-module/confirmation-dialog/confirmation-dialog.component';
import { OurTeamService } from 'src/app/backend/services/our-team/our-team.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-our-team-list',
  templateUrl: './our-team-list.component.html',
  styleUrls: ['./our-team-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class OurTeamListComponent implements OnInit, OnDestroy {
  loadingSearch = false;
  imageUrl: any = environment.apiUrl;

  query: IPagination = {
    limit: 5,
    total: 0,
    offset: 0,
    order: '-updatedAt',
    page: 1,
    filter: { filterText: '' },
  };

  ourTeamArray: any[] = [];
  language: any;
  _unsubscribeAll: Subject<any>;

  constructor(
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    public dialog: MatDialog,
    private ourTeamService: OurTeamService,
    public utilsService: UtilsService,
    public spinner: NgxSpinnerService,
    private loggedInUserService: LoggedInUserService,
  ) {
    this._unsubscribeAll = new Subject<any>();
    // ------------------------------------------------
    this.language = this.loggedInUserService.getLanguage() ? this.loggedInUserService.getLanguage().lang : 'es';
    // -------------------------------------------------
    this.spinner.show();
  }

  ngOnInit() {
    this.breadcrumbService.clearBreadcrumd();
    this.breadcrumbService.setBreadcrumd('Nuestro equipo', true);
    this.getOurTeamData();

    this.loggedInUserService.$languageChanged.pipe(takeUntil(this._unsubscribeAll)).subscribe((data: any) => {
      this.language = data.lang;
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next(true);
    this._unsubscribeAll.complete();
  }

  onEditOurTeam(id) {
    this.router.navigate(['/backend/our-team/edit', id]);
  }

  onDeleteMember(team) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '450px',
      data: {
        title: 'Confirmación',
        question: '¿Está seguro que desea eliminar este integrante?',
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.ourTeamService
          .removeTeam(team)
          .then(() => {
            this.query.limit = 5;
            this.query.offset = 0;
            this.query.total = 0;
            this.ourTeamArray = [];
            this.getOurTeamData();
          })
          .catch((error) => {
            this.utilsService.errorHandle(error);
          });
      }
    });
  }

  public getOurTeamData(): void {
    this.loadingSearch = true;
    this.ourTeamService.getAllTeamMembers(this.query).subscribe(
      (data) => {
        this.ourTeamArray = this.ourTeamArray.concat(data.data.flat());
        this.query.offset += data.meta.pagination.count;
        this.query.total = data.meta.pagination.total;
        this.loadingSearch = false;
        this.spinner.hide();
      },
      (error) => {
        this.utilsService.errorHandle(error);
        this.loadingSearch = false;
        this.spinner.hide();
      },
    );
  }

  onGetMore() {
    this.getOurTeamData();
  }

  public onChangeSorting(val) {
    let value = val.split('-');
    let orden = 1;
    let field = null;
    if (value.length > 1) {
      orden = 1;
      ``;
      field = value[1];
    } else {
      orden = -1;
      field = value[0];
    }
    this.ourTeamArray = this.ourTeamArray.sort(function (a, b) {
      if (a[field] < b[field]) {
        return orden;
      }
      if (a[field] > b[field]) {
        return -1 * orden;
      } else {
        return 0;
      }
    });
  }
}
