import { Component, OnInit, OnDestroy } from '@angular/core';
import { UtilsService } from 'src/app/core/services/utils/utils.service';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';
import { takeUntil } from 'rxjs/operators';
import { BreadcrumbService } from '../../../common-layout-components/breadcrumd/service/breadcrumb.service';
import { environment } from 'src/environments/environment';
import { ShowToastrService } from 'src/app/core/services/show-toastr/show-toastr.service';
import { OurTeamService } from 'src/app/backend/services/our-team/our-team.service';

@Component({
  selector: 'app-our-team-edit',
  templateUrl: './our-team-edit.component.html',
  styleUrls: ['./our-team-edit.component.scss'],
})
export class OurTeamEditComponent implements OnInit, OnDestroy {
  loadingSearch = false;
  loadImage = false;
  imageOurTeamChange = false;
  showErrorImage = false;
  urlImage = 'data:type/example;base64,';
  base64textString = null;
  imageOurTeam = null;
  compressimageOurTeam = null;
  form: FormGroup;
  language = null;
  _unsubscribeAll: Subject<any>;
  languages: any[] = [];
  languageForm: FormControl;
  selectedTeam: any;

  ///////////////////////////////////////////////////////////////
  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;

  constructor(
    public utilsService: UtilsService,
    private ourTeamService: OurTeamService,
    public spinner: NgxSpinnerService,
    private showToastr: ShowToastrService,
    private loggedInUserService: LoggedInUserService,
    private activatedRoute: ActivatedRoute,
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private fb: FormBuilder,
  ) {
    this._unsubscribeAll = new Subject();
    this.languages = this.loggedInUserService.getlaguages();
    const tempLang = this.languages.find(item => item.lang === 'es');
    this.languageForm = new FormControl(tempLang, []);
    this.language = 'es';
    this.loadingSearch = true;

    this.ourTeamService.getTeam(this.activatedRoute.snapshot.params.id).subscribe(
      data => {
        this.selectedTeam = data.data;
        this.form = this.fb.group({
          title: [this.selectedTeam.title, [Validators.required]],
          name: [this.selectedTeam.name, [Validators.required]],
          description: [this.selectedTeam.description, []],
        });
        if (this.selectedTeam.image) {
          this.imageOurTeam = environment.apiUrl + this.selectedTeam.image;
          this.loadImage = true;
        }
        this.loadingSearch = false;
      },
      error => {
        this.loadingSearch = false;
      },
    );
  }

  ngOnInit() {
    this.breadcrumbService.clearBreadcrumd();
    this.breadcrumbService.setBreadcrumd('Editar trabajador', true);

    this.languageForm.valueChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe(data => {
      this.language = data.lang;
    });
  }

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  /////////////////////////////////////

  // kike
  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];
    ///data:type/example;base64,
    this.urlImage = `data:${file.type};base64,`;
    if (files[0].size < 3000000) {
      if (files && file) {
        const reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      }
    } else {
      this.showErrorImage = true;
    }
  }

  async handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.imageOurTeam = this.urlImage + this.base64textString;
    try {
      this.imageOurTeam = this.imageOurTeam;
      this.loadImage = true;
      this.showErrorImage = false;
      this.imageOurTeamChange = true;
    } catch (error) {
      this.loadImage = true;
      this.showErrorImage = false;
      this.imageOurTeamChange = true;
    }
  }

  openFileBrowser(event) {
    event.preventDefault();
    const element: HTMLElement = document.getElementById('filePicker') as HTMLElement;
    element.click();
  }

  onCreateOurTeamMember() {
    this.spinner.show();
    let dataValue = this.form.value;
    dataValue.id = this.selectedTeam.id;
    if (this.imageOurTeamChange) {
      dataValue.image = this.imageOurTeam;
    }
    this.ourTeamService.editTeam(dataValue).subscribe(
      () => {
        this.showToastr.showSucces('Trabajador editado con éxito');
        this.spinner.hide();
        this.router.navigate(['/backend/our-team/']);
      },
      error => {
        this.spinner.hide();
        this.utilsService.errorHandle2(error);
      },
    );
  }

  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.lang === f2.lang;
  }

  parseLanguaje(data, lang) {
    data.title = { [lang]: data.title };
    data.text = { [lang]: data.text };
    return data;
  }

  parseLanguajeEdit(data, olData, lang) {
    olData.title[lang] = data.title;
    olData.text[lang] = data.text;
    data.title = olData.title;
    data.text = olData.text;
    return data;
  }

  //////////////////////////////////////////
  //////////////////////////////////////////
}
