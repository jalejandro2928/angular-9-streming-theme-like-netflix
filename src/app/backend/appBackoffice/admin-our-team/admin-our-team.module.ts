import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminOurTeamRoutingModule } from './admin-our-team-routing.module';
import { OurTeamEditComponent } from './our-team-edit/our-team-edit.component';
import { OurTeamCreateComponent } from './our-team-create/our-team-create.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SharedModule } from 'src/app/shared/shared.module';
import { ImageLazyLoadModule } from 'src/app/shared/image-lazy-load/image-lazy-load.module';
import { OurTeamListComponent } from './our-team-list/our-team-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    AdminOurTeamRoutingModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    ImageLazyLoadModule,
  ],
  declarations: [OurTeamCreateComponent, OurTeamEditComponent, OurTeamListComponent],
})
export class AdminOurTeamModule {}
