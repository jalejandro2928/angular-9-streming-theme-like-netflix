import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OurTeamListComponent } from './our-team-list/our-team-list.component';
import { OurTeamEditComponent } from './our-team-edit/our-team-edit.component';
import { OurTeamCreateComponent } from './our-team-create/our-team-create.component';

const routes: Routes = [
  { path: '', component: OurTeamListComponent },
  { path: 'edit/:id', component: OurTeamEditComponent },
  { path: 'create', component: OurTeamCreateComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminOurTeamRoutingModule {}
