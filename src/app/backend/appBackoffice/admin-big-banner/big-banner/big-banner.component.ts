import { environment } from 'src/environments/environment';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { CompressImageService } from 'src/app/core/services/image/compress-image.service';
import { MatDialog } from '@angular/material/dialog';
import { SettingService } from '../../../services/settings/settings.service';
import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConfirmationDialogComponent } from 'src/app/backend/common-dialogs-module/confirmation-dialog/confirmation-dialog.component';
import { ShowToastrService } from 'src/app/core/services/show-toastr/show-toastr.service';

@Component({
  selector: 'app-big-banner',
  templateUrl: './big-banner.component.html',
  styleUrls: ['./big-banner.component.scss']
})
export class BigBannerComponent implements OnInit {
  loadImage = false;
  imageBigPromoChange = false;
  showErrorImage = false;
  urlImage: string = 'data:type/example;base64,';
  base64textString = null;
  imageBigPromo = null;
  compressImageBigPromo: any = null;
  language = null;
  _unsubscribeAll: Subject<any>;
  languages: any[] = [];
  languageForm: FormControl;
  tags: any[] = [];
  imageUrl = environment.imageUrl;

  constructor(
    private compressImage: CompressImageService,
    public utilsService: UtilsService,
    public spinner: NgxSpinnerService,
    private dialog: MatDialog,
    private showTeastr: ShowToastrService,
    private settingService: SettingService
  ) {}

  ngOnInit() {
    this.settingService.getSetting().subscribe(data => {
      if (data.data.length) {
        this.base64textString = data.data[0].image;
        this.imageBigPromo = this.imageUrl + this.base64textString;
        this.loadImage = true;
      }
    });
  }

  /////////////////////////////////////

  // kike
  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];
    ///data:type/example;base64,
    this.urlImage = `data:${file.type};base64,`;
    if (files[0].size < 3000000) {
      if (files && file) {
        const reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      }
    } else {
      this.showErrorImage = true;
    }
  }

  async handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.imageBigPromo = this.urlImage + this.base64textString;
    try {
      this.imageBigPromo = this.imageBigPromo;
      this.loadImage = true;
      this.showErrorImage = false;
      this.imageBigPromoChange = true;
    } catch (error) {
      this.loadImage = true;
      this.showErrorImage = false;
      this.imageBigPromoChange = true;
    }

    // console.log(this.compressImageBigPromo.length);
  }

  openFileBrowser(event) {
    event.preventDefault();

    const element: HTMLElement = document.getElementById(
      'filePicker'
    ) as HTMLElement;
    element.click();
  }

  onSave() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '450px',
      data: {
        title: 'Confirmación',
        question: '¿Está seguro que desea guardar este cambio?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.spinner.show();
        this.settingService
          .editSettings({ image: this.imageBigPromo })
          .subscribe(
            data => {
              this.showTeastr.showSucces(
                'Big Banner promo successfully created'
              );
              this.spinner.hide();
            },
            error => {
              this.spinner.hide();
            }
          );
      }
    });
  }
}
