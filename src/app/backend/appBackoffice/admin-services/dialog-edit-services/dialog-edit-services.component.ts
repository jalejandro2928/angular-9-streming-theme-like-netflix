import { OurServicesService } from '../../../services/our-services/our-services.service';
import {
  Component,
  Inject,
  HostListener,
  ViewEncapsulation,
  OnInit,
  OnDestroy,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

import { environment } from 'src/environments/environment';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';
import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { Subject } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged, startWith } from 'rxjs/operators';

import { IconsDb } from '../classes/icons';
import { ShowToastrService } from 'src/app/core/services/show-toastr/show-toastr.service';

@Component({
  selector: 'app-dialog-edit-services',
  templateUrl: './dialog-edit-services.component.html',
  styleUrls: ['./dialog-edit-services.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DialogEditServicesComponent implements OnInit, OnDestroy {
  isSaving = false;
  isEditing = false;
  loggedInUser: any;
  innerWidth: any;
  applyStyle = false;
  form: FormGroup;
  languages: any[] = [];
  imageUrl: any;
  languageForm: FormControl;
  language: any;
  _unsubscribeAll: Subject<any>;
  selectedService = null;
  arrayIcons: any[] = [];
  filteredIcons: any[] = [];

  showErrorImage = false;
  urlImage = 'url(data:image/jpeg;base64,';
  base64textString = null;
  imageService = null;
  compressImageService = null;
  imageServiceChange = false;
  loadImage = false;
  selectedIcon = null;
  currentIndex = 20;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogEditServicesComponent>,
    private loggedInUserService: LoggedInUserService,
    private fb: FormBuilder,
    public spinner: NgxSpinnerService,
    public utilsService: UtilsService,
    private showTeastr: ShowToastrService,
    private ourServices: OurServicesService,
  ) {
    this.dialogRef.disableClose = true;
    this.loggedInUser = this.loggedInUserService.getLoggedInUser();
    this._unsubscribeAll = new Subject<any>();

    this.isEditing = data.isEditing;
    this.selectedService = data.selectedService;
    console.log('TCL: DialogEditBannersComponent -> selectedService', this.selectedService);
    this.imageUrl = environment.imageUrl;

    // ------------------LANGUAGE INITIALIZATION----------------
    this.languages = this.loggedInUserService.getlaguages();
    this.language = this.loggedInUserService.getLanguage() ? this.loggedInUserService.getLanguage().lang : 'es';
    this.languageForm = new FormControl(
      this.loggedInUserService.getLanguage() ? this.loggedInUserService.getLanguage() : this.languages[0],
    );
    // -------------------------------------------------------------------------------------------------
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth > 600) {
      this.applyStyle = false;
    } else {
      this.applyStyle = true;
    }
  }

  ngOnInit(): void {
    this.createForm();
    //////////////////EVENT ASSOCIATED WITH CHANGE LANGUAGE////////////////////////////
    this.languageForm.valueChanges.pipe(takeUntil(this._unsubscribeAll)).subscribe(data => {
      this.language = data.lang;
    });

    this.arrayIcons = IconsDb.icons;
    this.filteredIcons = this.arrayIcons.slice(0, 20);
    //////////////////////////////////////////////

    this.form.controls.icon.valueChanges.pipe(debounceTime(250), distinctUntilChanged()).subscribe(data => {
      this.onSearhIcons(data);
    });

    if (this.selectedService && this.selectedService.icon) {
      this.form.controls.icon.setValue(this.selectedService.icon);
      this.selectedIcon = this.selectedService.icon + '';
    }
  }

  createForm(): void {
    if (this.isEditing) {
      this.form = this.fb.group({
        name: [
          this.selectedService && this.selectedService.name ? this.selectedService.name : null,
          [Validators.required],
        ],
        description: [
          this.selectedService && this.selectedService.description ? this.selectedService.description : null,
          [Validators.required],
        ],
        icon: [
          this.selectedService && this.selectedService.icon ? this.selectedService.icon : null,
          [Validators.required],
        ],
      });
      if (this.selectedService.image) {
        this.base64textString = this.selectedService.image;
        this.imageService = this.imageUrl + this.base64textString;
        this.loadImage = true;
      }
    } else {
      this.form = this.fb.group({
        name: [null, [Validators.required]],
        description: [null, [Validators.required]],
        icon: [null, [Validators.required]],
      });
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  onSave(): void {
    this.spinner.show();
    let data = this.form.value;

    if (this.imageServiceChange) {
      data.image = this.imageService;
    }

    if (!this.isEditing) {
      this.ourServices.createService(data).subscribe(
        () => {
          this.showTeastr.showSucces('Bicon successfully created');
          this.spinner.hide();
          this.dialogRef.close(true);
        },
        error => {
          console.log(error);
          this.utilsService.errorHandle(error, 'Bicon', 'Creating');
          this.spinner.hide();
        },
      );
    } else {
      data.id = this.selectedService.id;
      console.log(data);
      this.ourServices.editService(data).subscribe(
        () => {
          this.showTeastr.showSucces('Updated bicon successfully');
          this.spinner.hide();
          this.dialogRef.close(true);
        },
        error => {
          console.log(error);
          this.utilsService.errorHandle(error, 'Bicon', 'Editing');
          this.spinner.hide();
        },
      );
    }
  }

  onSelectedIcon(icon) {
    this.selectedIcon = icon + '';
    this.form.controls.icon.setValue(icon);
  }

  onSearhIcons(searchValue) {
    this.currentIndex = 0;
    let temp = this.arrayIcons.filter((item: string) => item.indexOf(searchValue, 0) >= 0);
    this.filteredIcons = temp.length >= 50 ? temp.slice(0, 50) : temp;
  }

  onLoadMoreIcons() {
    let temp = this.arrayIcons.slice(
      this.currentIndex,
      Math.min(this.currentIndex + 20, this.arrayIcons.length - this.currentIndex),
    );
    // console.log("TCL: DialogEditBiconComponent -> onLoadMoreIcons -> temp", temp)
    this.currentIndex += temp.length;
    // console.log("TCL: DialogEditBiconComponent -> onLoadMoreIcons -> currentIndex", this.currentIndex)
    this.filteredIcons = this.filteredIcons.concat(temp);
  }
  // kike
  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];
    ///data:type/example;base64,
    this.urlImage = `data:${file.type};base64,`;
    if (files[0].size < 3000000) {
      if (files && file) {
        const reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      }
    } else {
      this.showErrorImage = true;
    }
  }

  async handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.imageService = this.urlImage + this.base64textString;
    try {
      this.imageService = this.imageService;
      this.loadImage = true;
      this.showErrorImage = false;
      this.imageServiceChange = true;
    } catch (error) {
      this.loadImage = true;
      this.showErrorImage = false;
      this.imageServiceChange = true;
    }
  }

  openFileBrowser(event) {
    event.preventDefault();

    const element: HTMLElement = document.getElementById('filePicker') as HTMLElement;
    element.click();
  }

  //////////////////////////////////////////
}
