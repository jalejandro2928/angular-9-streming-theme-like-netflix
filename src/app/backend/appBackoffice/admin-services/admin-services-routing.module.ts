import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminServicesListComponent } from './admin-services-list/admin-services-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminServicesListComponent,
  },
  {
    path: '**',
    redirectTo: '',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminServicesRoutingModule {}
