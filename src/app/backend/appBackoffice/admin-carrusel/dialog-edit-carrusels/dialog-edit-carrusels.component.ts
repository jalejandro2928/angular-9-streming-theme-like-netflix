import {
  Component,
  Inject,
  HostListener,
  ViewEncapsulation,
  OnInit,
  OnDestroy,
  ElementRef,
  ViewChild
} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef
} from '@angular/material/dialog';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';

import { environment } from 'src/environments/environment';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';
import { UtilsService } from 'src/app/core/services/utils/utils.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CompressImageService } from 'src/app/core/services/image/compress-image.service';
import { CarruselService } from '../../../services/carrusel/carrusel.service';
import { ShowToastrService } from 'src/app/core/services/show-toastr/show-toastr.service';

@Component({
  selector: 'app-dialog-edit-carrusels',
  templateUrl: './dialog-edit-carrusels.component.html',
  styleUrls: ['./dialog-edit-carrusels.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DialogEditCarruselsComponent implements OnInit, OnDestroy {
  isSaving = false;
  isEditing = false;
  loggedInUser: any;
  innerWidth: any;
  applyStyle = false;
  form: FormGroup;
  languages: any[] = [];
  imageUrl: any;
  languageForm: FormControl;
  language: any;
  _unsubscribeAll: Subject<any>;
  compareFn: ((f1: any, f2: any) => boolean) | null = this.compareByValue;
  selectedCarrusel = null;

  showErrorImage = false;
  urlImage = 'data:type/example;base64,';
  base64textString = null;
  imageCarrusel = null;
  compressImageCarrusel = null;
  imageCarruselChange = false;
  loadImage = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DialogEditCarruselsComponent>,
    private loggedInUserService: LoggedInUserService,
    private fb: FormBuilder,
    public spinner: NgxSpinnerService,
    private showToastr: ShowToastrService,
    public utilsService: UtilsService,

    private carruselService: CarruselService,
    private compressImage: CompressImageService
  ) {
    this.dialogRef.disableClose = true;
    this.loggedInUser = this.loggedInUserService.getLoggedInUser();
    this._unsubscribeAll = new Subject<any>();

    this.isEditing = data.isEditing;
    this.selectedCarrusel = data.selectedCarrusel;
    console.log(
      'TCL: DialogEditBannersComponent -> selectedCarrusel',
      this.selectedCarrusel
    );
    this.imageUrl = environment.imageUrl;

    // ------------------LANGUAGE INITIALIZATION----------------
    this.languages = this.loggedInUserService.getlaguages();
    this.language = this.loggedInUserService.getLanguage()
      ? this.loggedInUserService.getLanguage().lang
      : 'es';
    this.languageForm = new FormControl(
      this.loggedInUserService.getLanguage()
        ? this.loggedInUserService.getLanguage()
        : this.languages[0]
    );
    // -------------------------------------------------------------------------------------------------
  }

  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth > 600) {
      this.applyStyle = false;
    } else {
      this.applyStyle = true;
    }
  }

  ngOnInit(): void {
    this.createForm();
    //////////////////EVENT ASSOCIATED WITH CHANGE LANGUAGE////////////////////////////
    this.languageForm.valueChanges
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(data => {
        this.language = data.lang;
      });
    //////////////////////////////////////////////
  }

  createForm(): void {
    if (this.isEditing) {
      this.form = this.fb.group({
        title: [
          this.selectedCarrusel && this.selectedCarrusel.title
            ? this.selectedCarrusel.title[this.language]
            : null,
          [Validators.required]
        ],
        subTitle: [
          this.selectedCarrusel && this.selectedCarrusel.subTitle
            ? this.selectedCarrusel.subTitle[this.language]
            : null,
          [Validators.required]
        ],
        link: [
          this.selectedCarrusel.link,
          [
            Validators.pattern(
              /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/
            )
          ]
        ]
      });
      if (this.selectedCarrusel.image) {
        this.base64textString = this.selectedCarrusel.image;
        this.imageCarrusel = this.imageUrl + this.base64textString;
        this.loadImage = true;
      }
    } else {
      this.form = this.fb.group({
        title: [null, [Validators.required]],
        subTitle: [null, [Validators.required]],
        link: [
          null,
          [
            Validators.pattern(
              /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/
            )
          ]
        ]
      });
    }
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // kike
  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];
    ///data:type/example;base64,
    this.urlImage = `data:${file.type};base64,`;
    if (files[0].size < 3000000) {
      if (files && file) {
        const reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
      }
    } else {
      this.showErrorImage = true;
    }
  }

  async handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.imageCarrusel = this.urlImage + this.base64textString;
    try {
      this.imageCarrusel = this.imageCarrusel;
      this.loadImage = true;
      this.showErrorImage = false;
      this.imageCarruselChange = true;
    } catch (error) {
      this.loadImage = true;
      this.showErrorImage = false;
      this.imageCarruselChange = true;
    }
  }

  openFileBrowser(event) {
    event.preventDefault();

    const element: HTMLElement = document.getElementById(
      'filePicker'
    ) as HTMLElement;
    element.click();
  }

  //////////////////////////////////////////

  //////////////////////////////////////////

  onSave(): void {
    this.spinner.show();
    let data = this.form.value;

    if (this.imageCarruselChange) {
      data.image = this.imageCarrusel;
    }

    if (!this.isEditing) {
      data = this.parseLanguaje(data, this.language);
      this.carruselService.createCarrusel(data).subscribe(
        () => {
          this.showToastr.showSucces('Carrusel successfully created');
          this.spinner.hide();
          this.dialogRef.close(true);
        },
        error => {
          console.log(error);
          this.utilsService.errorHandle(error, 'Carrusel', 'Creating');
          this.spinner.hide();
        }
      );
    } else {
      data = this.parseLanguajeEdit(data, this.selectedCarrusel, this.language);
      data.id = this.selectedCarrusel.id;
      console.log(data);
      this.carruselService.editCarrusel(data).subscribe(
        () => {
          this.showToastr.showSucces('Updated carrusel successfully');
          this.spinner.hide();
          this.dialogRef.close(true);
        },
        error => {
          console.log(error);
          this.utilsService.errorHandle(error, 'Carrusel', 'Editing');
          this.spinner.hide();
        }
      );
    }
  }

  ////////////////////////////UTILS FOR LANGUAGE HANDLE///////////////////////////////////////
  compareByValue(f1: any, f2: any) {
    return f1 && f2 && f1.lang === f2.lang;
  }

  parseLanguaje(data, lang) {
    data.title = { [lang]: data.title };
    data.subTitle = { [lang]: data.subTitle };
    return data;
  }

  parseLanguajeEdit(data, oldData, lang) {
    oldData.title[lang] = data.title;
    oldData.subTitle[lang] = data.subTitle;
    data.title = oldData.title;
    data.subTitle = oldData.subTitle;
    return data;
  }
  ///////////////////////////////////////////////////////////////////////////////////////////
}
