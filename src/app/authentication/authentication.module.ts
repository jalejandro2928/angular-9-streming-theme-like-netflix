import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './authentication/authentication.component';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';
import { RegisterComponent } from './register/register.component';

// --------MATERIAL MODULES------- //

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    AuthenticationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  declarations: [AuthenticationComponent, LoginComponent, RegisterComponent],
})
export class AuthenticationModule {}
