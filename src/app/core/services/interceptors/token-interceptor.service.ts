import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggedInUserService } from '../loggedInUser/logged-in-user.service';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  token: any = null;
  currency: any = null;
  language: any = null;

  constructor(private loggedInUserService: LoggedInUserService) {
    this.token = loggedInUserService.getTokenOfUser();
    this.currency = JSON.parse(localStorage.getItem('currency')) ? JSON.parse(localStorage.getItem('currency')).name : null;
    this.language = JSON.parse(localStorage.getItem('language')) ? JSON.parse(localStorage.getItem('language')).lang : null;

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.token = this.loggedInUserService.getTokenOfUser();
    this.currency = JSON.parse(localStorage.getItem('currency')) ? JSON.parse(localStorage.getItem('currency')).name : null;
    this.language = JSON.parse(localStorage.getItem('language')) ? JSON.parse(localStorage.getItem('language')).lang : null;
    if (this.token) {
      request = request.clone({
        setHeaders: {
          Authorization: this.token
        }
      });
    }
    if (this.language) {
      request = request.clone({
        setHeaders: {
          language: this.language
        }
      });
    }
    if (this.currency) {
      request = request.clone({
        setHeaders: {
          currency: this.currency
        }
      });
    }
    return next.handle(request);
  }


}
