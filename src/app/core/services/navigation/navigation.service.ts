import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class NavigationService {
  navBackend: any[] = [
    {
      displayName: 'Productos',
      iconName: ['storefront'],
      route: 'backend/product/list',
      material: true,
      children: [],
    },
    {
      displayName: 'Categorías',
      iconName: ['category'],
      route: 'backend/product/categories',
      material: true,
      children: [],
    },
    {
      displayName: 'Marcas',
      iconName: ['branding_watermark'],
      route: 'backend/product/brands',
      material: true,
      children: [],
    },

    {
      displayName: 'Publicaciones',
      iconName: ['note'],
      route: 'backend/blog/list',
      material: true,
      children: [],
    },
    {
      displayName: 'Nuestro Equipo',
      iconName: ['work'],
      route: 'backend/our-team',
      material: true,
      children: [],
    },
    {
      displayName: 'Nuestros servicios',
      iconName: ['shop_two'],
      route: 'backend/services',
      material: true,
      children: [],
    },
    // {
    //   displayName: 'Carruseles',
    //   iconName: ['featured_video'],
    //   route: 'backend/carrusel',
    //   material: true,
    //   children: [],
    // },

    // {
    //   displayName: 'Pancartas',
    //   iconName: ['perm_media'],
    //   route: 'backend/banners',
    //   material: true,
    //   children: []
    // },
    // {
    //   displayName: 'Paralax promocionales',
    //   iconName: ['perm_media'],
    //   route: 'backend/big-banner',
    //   material: true,
    //   children: [],
    //   divider: true
    // },
    {
      displayName: 'Usuarios',
      iconName: ['person'],
      route: 'backend/users',
      material: true,
      children: [],
    },
  ];

  constructor() {}

  public getNavBackend() {
    return JSON.parse(JSON.stringify(this.navBackend));
  }
}
