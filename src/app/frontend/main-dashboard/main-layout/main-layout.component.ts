import { NavigationFrontService } from '../services/navigation/navigation-front.service';
import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { takeUntil } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MainLayoutComponent implements OnInit, OnDestroy {
  loggedInUser: any = undefined;
  navItems: any[] = [];
  _unsubscribeAll: Subject<any>;
  urlImage: any = environment.imageUrl;
  flag: any = undefined;

  public flags = [
    { name: 'Español', image: 'assets/images/flags/es.svg', lang: 'es' },
    { name: 'English', image: 'assets/images/flags/en.svg', lang: 'en' },
  ];

  constructor(
    public loggedInUserService: LoggedInUserService,
    public navigationServ: NavigationFrontService,
    private translate: TranslateService,
  ) {
    this.navItems = this.navigationServ.getNavItems();
    this._unsubscribeAll = new Subject<any>();
    this.loggedInUser = this.loggedInUserService.getLoggedInUser();
  }

  ngOnInit() {
    const tempFlag = JSON.parse(localStorage.getItem('language'));
    this.flag = tempFlag ? tempFlag : this.flags[0];

    /////// Subscribe to events //////////
    this.loggedInUserService.$loggedInUserUpdated.pipe(takeUntil(this._unsubscribeAll)).subscribe((data) => {
      this.loggedInUser = this.loggedInUserService.getLoggedInUser();
      const defaultLanguage: any = { name: 'Español', image: 'assets/images/flags/es.svg', lang: 'es' };
      if ('language' in localStorage) {
        let language = JSON.parse(localStorage.getItem('language'));
        language = language ? language : defaultLanguage;
        this.translate.setDefaultLang(language.lang);
        this.translate.use(language.lang);
      } else {
        this.translate.setDefaultLang(defaultLanguage.lang);
        localStorage.setItem('language', JSON.stringify(defaultLanguage));
      }
    });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  onChangePass() {}

  onLogout() {}

  onShowProfile() {}

  onGotoBackend() {}

  public changeLang(flag) {
    this.translate.use(flag.lang);
    localStorage.setItem('language', JSON.stringify(flag));
    this.flag = flag;
    this.loggedInUserService.$languageChanged.next(flag);
  }
}
