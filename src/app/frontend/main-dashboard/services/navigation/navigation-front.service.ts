import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class NavigationFrontService {
  navItems: any[] = [
    {
      displayName: 'Home',
      iconName: ['aspect_ratio'],
      material: true,
      route: '/home',
      disabled: false,
    },
    {
      displayName: 'Programs',
      iconName: ['dvr'],
      route: '#',
      material: true,
      disabled: false,
    },

    {
      displayName: 'Movies',
      iconName: ['movie'],
      route: '#',
      material: true,
      disabled: false,
    },

    {
      displayName: 'Most recent',
      iconName: ['recent_actors'],
      route: '#',
      material: true,
      divider: true,
      disabled: false,
    },
    {
      displayName: 'My list',
      iconName: ['list'],
      material: true,
      route: '#',
      disabled: false,
    },
    {
      displayName: 'About Us',
      iconName: ['group'],
      route: '/about',
      material: true,
      disabled: false,
    },
    {
      displayName: 'FAQ',
      iconName: ['help'],
      route: '/faq',
      material: true,
      divider: true,
      disabled: false,
    },
    {
      displayName: 'My Account',
      iconName: ['perm_identity'],
      route: '//my-account',
      material: true,
      disabled: false,
    },
  ];

  constructor() {}

  public getNavItems() {
    return this.navItems;
  }
}
