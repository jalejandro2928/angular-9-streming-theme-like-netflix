import { FlexLayoutModule } from '@angular/flex-layout';
import { MatListModule } from '@angular/material/list';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainDashboardRoutingModule } from './main-dashboard-routing.module';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MatBadgeModule } from '@angular/material/badge';
import { SidebarMenuService } from './sidebar/sidebar-menu.service';
import { MainHomeComponent } from './main-home/main-home.component';
import { CarruselsVideosComponent } from './carrusels-videos/carrusels-videos.component';
import { SwiperModule } from 'ngx-swiper-wrapper';
@NgModule({
  declarations: [MainLayoutComponent, SidebarComponent, MainHomeComponent, CarruselsVideosComponent],
  imports: [
    CommonModule,
    MainDashboardRoutingModule,
    SharedModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    TranslateModule,
    MatBadgeModule,
    MatListModule,
    FlexLayoutModule,
    SwiperModule,
  ],
  providers: [SidebarMenuService],
})
export class MainDashboardModule {}
