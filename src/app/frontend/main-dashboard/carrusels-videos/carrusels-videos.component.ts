import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'app-carrusels-videos',
  templateUrl: './carrusels-videos.component.html',
  styleUrls: ['./carrusels-videos.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CarruselsVideosComponent implements OnInit, AfterViewInit {
  index = 1;
  config: SwiperConfigInterface = {};
  hoverIntervalTime;
  countForHover = 0;
  showPreviewState = false;
  xpos = 0;
  ypos = 0;

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.config = {
      observer: true,
      slidesPerView: 'auto',
      spaceBetween: 4,
      keyboard: true,
      navigation: true,
      pagination: true,
      loop: false,
      preloadImages: false,
      updateOnWindowResize: true,
      direction: 'horizontal',
      lazy: true,
      autoplay: false,
      watchOverflow: true,
      loopedSlides: null,
      breakpoints: {
        500: {
          slidesPerView: 2,
        },
        720: {
          slidesPerView: 'auto',
        },
      },
    };
  }

  onPrevItem() {
    console.log("asda;sds'd");
  }

  onNextItem() {
    console.log('xaxaxaaxaxa');
  }

  onMouseEnter(event, id) {
    if (!this.showPreviewState) {
      this.hoverIntervalTime = setInterval(() => {
        this.countForHover++;
        if (this.countForHover > 10) {
          this.showPreviewState = true;

          // setTimeout(() => {
          //   let element = document.getElementById('hoverLayout');
          //   element.style.left = event.x - 180 + 'px';
          // }, 10);

          this.countForHover = 0;
          console.log('talla1');
          clearInterval(this.hoverIntervalTime);
        }
      }, 75);
    } else {
      console.log('pepe selecciono el hover state');
    }
  }

  onMouseLeave(id) {
    if (this.hoverIntervalTime) {
      clearInterval(this.hoverIntervalTime);
      this.countForHover = 0;
    }
  }

  onClosePreview() {
    if (this.hoverIntervalTime) {
      clearInterval(this.hoverIntervalTime);
    }
    this.showPreviewState = false;
  }

  onPlay() {
    this.showPreviewState = true;
  }
}
