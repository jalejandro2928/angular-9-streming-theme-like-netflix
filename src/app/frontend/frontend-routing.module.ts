import { MainHomePageComponent } from './main-home-page/main-home-page.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: MainHomePageComponent },
  {
    path: 'home',
    loadChildren: () => import('./main-dashboard/main-dashboard.module').then((m) => m.MainDashboardModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrontendRoutingModule {}
