import { environment } from './../../../environments/environment.prod';
import { LoggedInUserService } from 'src/app/core/services/loggedInUser/logged-in-user.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormControl, Validators, Form, FormGroup, FormBuilder } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { UtilsService } from 'src/app/core/services/utils/utils.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit, OnDestroy {
  loggedInUser: any = null;
  _unsubscribeAll: Subject<any> = new Subject();
  form: FormGroup;
  subscribeState = false;
  version = '';

  constructor(
    private loggedInUserService: LoggedInUserService,
    private httpClient: HttpClient,
    private utilsService: UtilsService,
    private fb: FormBuilder,
    private router: Router,
  ) {
    //isSubscribed//
    this.loggedInUser = this.loggedInUserService.getLoggedInUser();

    // const tempStorage = JSON.parse(localStorage.getItem('subscribeState'));
    this.subscribeState = this.loggedInUser ? this.loggedInUser.isSubscribed : false;
    localStorage.setItem('subscribeState', JSON.stringify(this.subscribeState));
    this.version = environment.version;

    this.form = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
